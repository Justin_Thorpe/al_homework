/**
 * compares strings
*/    
public class Driver
{
   public static void main(String[] args)
   {
      String a = "fun";
      String b = "funny";
      System.out.println(cmp_str(a,b));
   
   }
   
   public static int cmp_str(String a, String b)
   {
      int i=0;
      while(i<a.length() && i<b.length()){
         if(a.charAt(i)>b.charAt(i)){
            return -1;
         }else if(a.charAt(i)<b.charAt(i)){
            return 1;
         }else{
            i++;
         }
      }  
      if(a.length()>b.length()){
         return 1;
      }else if(a.length()<b.length()){
         return -1;
      }else{
         return 0;
      }                    
   }
}      
                                                   