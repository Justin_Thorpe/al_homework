;;
;; strmacro.asm
;; 
;; macro that compares strings
;;
;; by Justin T.

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

;; cmp_str str, str
;;
;; Compares two char arrays
;;
;; str must be a 64-bit address; label or reg
%macro	cmp_str 2
	pop	rax
	pop	rdi
	pop	rbx
	pop	rcx
	mov	rdi, 0
check:
	mov	rbx, %1
	mov	rcx, %2
	mov     bl, byte[rbx+rdi]
	mov	cl, byte[rcx+rdi]
	cmp	bl, 0
	je	doneA
	cmp	cl, 0
	je	doneB
	cmp	bl, cl
	je	inc
	cmp	bl, cl
	jg	greater
	jmp	less
inc:
	inc	rdi
	jmp 	check
equal:
	mov	rax,0
	jmp	end
greater:
	mov	rax, 1
	jmp	end
less:
	mov	rax, -1
	jmp	end
doneA:
	cmp	cl, 0
	jg	greater
	cmp	cl, 0
	je	equal
doneB:
	cmp	bl, 0
	jg	less
	cmp	bl, 0
	je	equal
end:
	push	rcx
	push	rbx
	push	rdi
%endmacro

	global main
		
	section .data
endl:	db	10, 0
a:	db	"fun", 0
b:	db	"fun", 0

	section .text
main:
	;dump_regs
	cmp_str	a, b
	;dump_regs
	mov	[rbx],rax
	put_i	[rbx]
	put_str	endl
	
	mov	eax, 60
	xor	rdi, rdi
	syscall
