;
; labday.asm
;
; Playing around with the stack.
;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

;; madd val1, val2
;;
;; Adds, even memory to memory
;;
;; either val can be (label, reg, imm)
%macro	madd	2
	pop	rax
	pop	rbx
	mov	rax, %1
	mov	rbx, %2
	add	eax, ebx
	push	rax
	pop	%1
	pop	rbx
	pop	rax
%endmacro
	  
		global main

		section	.data
endl:		db	10,0
a:		dd	12
b:		dd	15

		section .text

main:
		;mov	rax, 0
		;mov	rbx, 2
		madd	qword[a],qword[b]
		put_i	[a]
		put_str	endl
		put_i	[rax]
		put_str	endl

		mov     eax, 60                 ; system call 60 is exit
		xor     rdi, rdi                ; exit code 0
		syscall
