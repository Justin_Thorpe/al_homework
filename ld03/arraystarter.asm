;
; arraystarter.asm
;
; Set up a simple array of ints in memory and then play around with it.
;
; @author  Terry Sergeant
; @version Fall 2016
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		section .data
myarray:	dd	0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA
endl		db	10,0
numb:		db	"numbers.txt",0
		section .bss
fp:	resq	1
a:	resd	100
		section .text
		global 		main
main:
		mov     r8, numb
                fopenr  [fp], r8
		xor	edi, edi
		mov	edi, 0
		mov	edx, a
check:
		
		fget_i  [fp], [edx+4*edi]
		cmp eax, -1
		je	print
		;put_i	[edx+4*edi]
		;put_str	endl
		inc	edi	
		jmp	check

		
print:
		jmp 	loop
loop:
		put_i	[edx+4*edi]
		put_str	endl
		inc	edi
		jmp	print







end:
                fclosem [fp]
		; before running this guess what the output will be ... THEN run it.
		;put_i	[myarray]
		;put_str	endl

		; write the necessary statements, without using a loop, to display all elements of myarray
		;xor 	rax, rax
		;mov	rax, myarray
		;mov	ebx, [rax]
		;put_i	ebx
	;	put_str	endl
	;	mov	ebx, [rax+4]
	;	put_i	ebx
	;	put_str	endl
	;	mov     ebx, [rax+8]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+12]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+16]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+20]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+24]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+28]
         ;       put_i   ebx
          ;      put_str endl
	;	mov     ebx, [rax+32]
           ;     put_i   ebx
         ;       put_str endl
	;	mov     ebx, [rax+36]
         ;       put_i   ebx
	
	; now display myarray using a loop
	;	xor	rax, rax
	;	mov	rax, myarray
	;	mov	r8d, 0
;check:		
	;	cmp	r8d, 10
	;	jl	loop
	;	jmp	alldone

;loop:
	;	mov	ebx, [rax]
	;	add 	rax, 4
	;	put_i	ebx
	;	put_str	endl
	;	inc	r8d
	;	jmp	check



		; bring a printout of your completed source code to labday

alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

