;;
;; longlivethedog.asm
;;
;; Calculates the "human" age of dogs with given input by user.
;;
;; by Justin Thorpe
;;

%include "../lib/iomacros.asm"

	global main
	section .data

ena_str: db "Enter name of dog: ",0
ent_str: db "How old is ",0
dog_str: db "veryverylongnameforadogwhyisitthislonglikereally",0
spec: db ": ",0
is_str: db " is ",0
in_str: db " in dog years",0

myint: dd 123
endl: db 10,0	
	
	section .text

main:		

	put_str  ena_str	; Ask user name of dog
	get_str	 dog_str 	; Gets dog name as input from user
	put_str  endl

	put_str  ent_str	; Ask user age of dog
	put_str  dog_str
	put_str  spec
	get_i  [myint]		; Gets user input for dog's age
	put_str endl

	mov	ecx, [myint]	;; Stores user's dog age in ecx
	imul	ecx, 7		;; Multiplies user's dogs age by 7
	mov	[myint], ecx	;; Stores new age in "myint"

	put_str  dog_str	;; ------------------------------------
	put_str  is_str		;;
	put_i  [myint]		;; Prints user's dog's age in dog years
	put_str  in_str		;;
	put_str  endl		;; ------------------------------------	

	mov	eax, 60
	xor	rdi, rdi
	syscall 
