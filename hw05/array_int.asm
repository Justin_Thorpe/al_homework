;;
;; array_int.asm
;;
;; Stores 32 bit ints read from a file into an array and performs calculations
;;
;; By Justin T.
;;

%include "../lib/iomacros.asm"

	global main
	section .data

endl:	db	10,0
file:	db	"s_int.txt",0
bfile:	db	"../data/numbers.txt",0
t:	dd	0
max:	db	"Maximum value: ",0
min:	db	"Minimum value: ",0
avg:	db	"Average value: ",0
	section .bss

fp:	resq	1
a:	resd	10000
	section .text

main:
	;mov	r8,file
	mov	r8,bfile
	fopenr	[fp],r8
	mov	edi,0
	mov	ecx,a
	mov	esi,0
check:
	fget_i	[fp],[ecx+4*edi]
	cmp	eax,-1
	je	store
	inc	edi
	jmp	check
store:
	mov	dword[t],edi
	dec	dword[t]
	mov	r13d,0
	jmp	sort
loop:
	inc	r13d
	mov	esi,0
	cmp	r13d,edi
	jg	close
	jmp	sort	
sort:
	cmp	esi,dword[t]
	je	loop
	mov	r11d,[ecx+4*esi]
	mov	r12d,[ecx+4*esi+4]
	cmp	r11d,r12d
	jg	swap
	inc	esi
	jmp	sort
swap:
	mov	r9d,[ecx+4*esi]
	mov	r10d,[ecx+4*esi+4]
	mov	[ecx+4*esi],r10d
	mov     [ecx+4*esi+4],r9d
	inc	esi
	jmp	sort	
add:
	cmp	esi,edi
	je	print
	add	r15d,[ecx+4*esi]		
	inc	esi
	jmp	add
print:
	mov	eax, r15d
	cdq
	idiv	edi	
	mov	r14d,eax

	mov	esi,dword[t]
	put_str	max
	put_i	[ecx+4*esi]
	put_str endl
	
	put_str	min
	put_i	[ecx]
	put_str	endl
	
	put_str	avg
	put_i	r14d
	put_str	endl
	jmp	exit
close:
	fclosem	[fp]
	xor	esi,esi
	xor	r15d,r15d
	jmp	add


exit:
	mov	ebx,0
	mov	eax,1
	int	80h
