;;
;; array_str.asm
;;
;; stores in an array strings(char values) adn displays names with 20+ char
;;
;; By Justin T.
;;

%include "../lib/iomacros.asm"

	global main	
	section .data

endl:	db	10,0
file:	db	"s_words.txt",0
bfile:	db	"../data/names.txt",0
	section .bss

fp:	resq	1
a:	resb	300000 ;30*10000
b:	resd	10000
	section .text
main:
	;mov	r8,file
	mov	r8,bfile
	fopenr	[fp],r8
	mov	edi,0
	mov	rdx,a
	mov	esi,0
	mov	r10d,b
check:
	fget_str [fp],rdx
	cmp	eax,-1
	je	close
	mov	[r10d+4*edi],eax
	add	rdx,30
	inc	edi
	jmp	check
close:
	fclosem	[fp]
	mov	rdx,a
	jmp	loop
loop:
	cmp	esi,edi
	je	exit
	cmp	dword[r10d+4*esi],20
	jge	print
	inc	esi
	add	rdx,30
	jmp	loop
print:
	put_str	rdx
	put_str	endl
	inc	esi
	add	rdx,30
	jmp	loop

exit:
	mov	ebx,0
	mov	eax,1
	int	80h
