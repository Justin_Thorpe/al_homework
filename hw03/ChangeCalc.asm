;; 
;; ChangeCalc.asm
;;
;; Calculates the amount of coins to be given based on user input and after a fee
;;
;; by Justin Thorpe
;;

%include "../lib/iomacros.asm"

	global main
	section .data

ochange_int: dd 0
fee_int: dd 0
rchange_int: dd 0
quarter_int: dd 0
dime_int: dd 0
nickel_int: dd 0
penny_int: dd 0

ask_str: db "Enter change (0-99): ",0
sf_str: db "Service fee: ",0
rc_str: db "Remaining change: ",0
quarter_str: db "Number of Quarters: ",0
dime_str: db "Number of Dimes: ",0
nickel_str: db "Number of Nickels: ",0
penny_str: db "Number of Pennies: ",0
endl: db 10,0

	section .text

main:

	put_str  ask_str	; Ask user to input original change amount
	get_i  [ochange_int]	; Gets user's input for original change

	mov	ecx, [ochange_int]
	imul	ecx, 5
	mov	eax, ecx
	xor	edx, edx
	mov	edi, 100
	idiv	edi
	
	mov	[fee_int], eax
	mov	esi, [ochange_int]
	sub	esi, [fee_int]
	mov	[rchange_int], esi

	xor	eax, eax
	xor	edx, edx
	xor	ecx, ecx
	xor	edi, edi
	xor	esi, esi
	mov	edi, 25
	mov	esi, 10
	mov	ebx, 5
	mov	ecx, 1
	mov	eax, [rchange_int]
	idiv	edi
	mov	[quarter_int], eax
	mov	r10d, edx
	
	xor	edx, edx
	xor	eax, eax
	mov	eax, r10d
	idiv	esi
	mov	[dime_int], eax
	xor	r10d, r10d
	mov	r10d, edx
	
	xor	edx, edx
	xor	eax, eax
	mov	eax, r10d
	idiv	ebx
	mov	[nickel_int], eax
	xor	r10d, r10d
	mov	r10d, edx

	xor 	edx, edx
	xor 	eax, eax
	mov	eax, r10d
	idiv	ecx
	mov	[penny_int], eax

	put_str  sf_str
	put_i  [fee_int]	
	put_str endl

	put_str  rc_str
	put_i  [rchange_int]
	put_str  endl

	put_str  quarter_str
	put_i	[quarter_int]
	put_str  endl

	put_str  dime_str
	put_i  [dime_int]
	put_str  endl

	put_str  nickel_str
	put_i	[nickel_int]
	put_str  endl
	
	put_str  penny_str
	put_i  [penny_int]
	put_str  endl 	

	mov	eax, 60
	xor	rdi, rdi
	syscall
