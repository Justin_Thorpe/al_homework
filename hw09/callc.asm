;
; callc.asm
;
; Demonstrate calling a C-language function.
;
; @author  Terry Sergeant
; @version Fall 2016
;
; To complile this code:
;   gcc -c display.c               # produces display.o
;   nasm -f elf64 callc.asm        # produces callc.o
;   gcc callc.o display.o          # produces a.out

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		extern readStrings	; tells nasm that displayNums is defined externally
		extern findSuccPos	; tells nasm that eh finsucc is defined externally
		section .data
ints:		dd	68,-9,23,45,67,78,32,31,525,123 ; an array of 10 ints
n:		dd	10	; number of ints in the array
endl:		db	10,0
file:		db	"../data/names.txt",0
		section .bss
a:		resb	27*2501
section .text
		global 		main
main:
		xor 	rax,rax
		mov	rdi,a		; rdi= param 1 which is address of array
		xor	rsi,rsi		; rsi= second params which is file name
		mov	rsi,file
		call	readStrings	; call externally defined displayNums
		put_i	eax
		put_str	endl
		put_str	file
		put_str	endl
		mov	rdi, a
		mov	rsi, rax
		call	findLongPos
		put_i	eax
		put_str	endl
		imul	rax, 27	
		mov	rdi, a
		add	rdi, rax
		put_str	rdi
		put_str	endl

alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit
findLongPos:
		mov	r14, 0
		xor	rdx, rdx	; keeps track of index for r12
		xor	rcx, rcx	; keeps track of index for r13

		call	strlen
		mov	r12, rax
		mov	rdx, 0
		add	rdi, 27
		call	strlen
		mov	r13, rax
		mov	rcx, 1
		
compare:
		cmp	r14, rsi
		jge	exit
		cmp	r12, r13
		jge	first
		
		add	rdi, 27
		call	strlen
		mov	r12, rax
		inc	r14
		mov	rdx, r14
		jmp	compare
first:
		add	rdi, 27
		call 	strlen
		mov	r13, rax
		inc	r14
		mov	rcx, r14
		jmp	compare
exit:
		cmp	r12, r13
		jge	ff
		mov	rax, rcx
		ret
ff:
		mov	rax, rdx
		inc	rax
		ret
strlen:
		mov	r15, 0
loop:
		cmp	byte[rdi+r15],0
		je	done
		inc	r15
		jmp	loop
done:		
		dec	r15
		mov	rax, r15
		ret	
