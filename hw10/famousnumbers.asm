;
; Floating Point Calculations
;
; @author  Justin T.
;

%include "../lib/iomacros.asm"
		extern fabs
		section .data
pi:		dq	3.141592653589793       ; PI
cutoff:		dq	0.0000001		; cutoff
one:		dq	1.0			;initial numerator as well as, well the number one
s_denom:	dq	3.0			;initial denominator
two:		dq	2.0			
zero:		dq	0.0
six:		dq	6.0
ais:		db	"Estimation for pi: : ",0
endl:		db	10,0
		
		section .bss
ans:		resq	1
		
		section .text

		global 	main
main:
		
		movsd	xmm8,[pi]	; goal
		movsd	xmm9,[zero]	; starting estimate
		movsd	xmm13, [zero]	; counter	
		movsd	xmm10,[cutoff]	; cutoff
loop:
		movsd	xmm11, xmm9	;temp
		subsd	xmm11, xmm8
		movsd	xmm0, xmm11
		call	fabs
		ucomisd	xmm0, xmm10
		jbe	alldone
		movsd	xmm0, xmm13
		call	estimate_pi
		addsd	xmm13, [one]
		movsd	xmm9, xmm0
		movsd	[ans], xmm9
		put_str	ais
		put_dbl	[ans]
		put_str	endl	
		jmp	loop

		
		

alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit
estimate_pi:
		ucomisd	xmm0, [zero]
		je	zed
		movsd	xmm15, xmm0
		movsd	xmm6, xmm15
		movsd	xmm7, xmm6
		mulsd	xmm7, [two]
		addsd 	xmm7, [one]
		divsd	xmm6, xmm7	; fraction at n
		addsd	xmm6, [one]	; adds one to fractioon at n
		movsd	xmm3, xmm6
		subsd	xmm15,	[one]

check:
		ucomisd	xmm15, [zero]
		jbe	last
		movsd	xmm6, xmm15
		movsd   xmm7, xmm6
                mulsd   xmm7, [two]
                addsd   xmm7, [one]
                divsd   xmm6, xmm7      ; fraction at n
                mulsd	xmm3, xmm6
		addsd	xmm3, [one]
		subsd	xmm15, [one]
		jmp	check
last:
		addsd	xmm3, [one]
		mulsd	xmm3, [two]
		movsd	xmm0, xmm3
		subsd	xmm0, [two]
		ret
zed:
	movsd	xmm0, [two]
	ret
