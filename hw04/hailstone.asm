;;
;; hailstone.asm
;;
;; Calculates length of the hailstone sequence for 2-100k
;;
;; By Justin T.
;;

%include "../lib/iomacros.asm"

	global main
	section .data

hsl_str: db " has length: ",0
n_int: dd 2
ph_int: dd 2
length_int: dd 0
endl:  db 10,0

	section .text

main:
	jmp	start
start:
	mov	dword[length_int], 0

	mov	esi, 100000
	cmp	[n_int], esi
	je	end

	mov	eax, [n_int]
	mov	[ph_int], eax
	jmp	check
check:	
	mov	edi, 1
	cmp	[ph_int], edi
	je	next

	xor	edx, edx
	mov	eax, [ph_int]
	mov	ebx, 2
	idiv	ebx

	cmp	edx, 0
	jg	odd
	
	inc	dword[length_int]
	mov     [ph_int], eax
	jmp	check


odd:
	inc	dword[length_int]
	xor	edx, edx
	mov	eax, [ph_int]
	mov	ecx, 3
	imul	ecx
	mov	[ph_int], eax
	inc	dword[ph_int]
	jmp	check

next:
	mov	edi, 300
	cmp	[length_int], edi
	jge	print
	inc	dword[n_int]
	jmp	start
print:
	put_i  [n_int]
	put_str  hsl_str
	put_i  [length_int]
	put_str  endl
	inc	dword[n_int]
	jmp	start

end:
	mov	eax, 60
	xor	rdi, rdi
	syscall	 
