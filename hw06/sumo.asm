;;
;; sumo.asm
;;
;; stores in an array strings(char values) adn displays names with 20+ char
;;
;; By Justin T.
;;

%include "../lib/iomacros.asm"
%define	RANK	20
%define	HEIGHT	24
%define	WEIGHT	28
%define	RECSIZE	32

	global main	
	section .data

endl:	db	10,0
sfile:	db	"../data/short.txt",0
file:	db	"../data/sumo.txt",0
tall:	db	"The Tallest Rank is: ",0
heavy:	db	"The Heaviest Rank is: ",0
	section .bss
fp:	resq	1
s:	resb	RECSIZE*100
temp:	resb	RECSIZE
i:	resd	1
j:	resd	1	

	section .text
main:
	;mov	r8,sfile
	mov	r8,file
	fopenr	[fp],r8
	mov	edi,0
	mov	esi,0
	mov	rdx,s
load:
	fget_i [fp],[rdx+RANK]
	fget_i [fp],[rdx+HEIGHT]
	fget_i [fp],[rdx+WEIGHT]
	fget_ch [fp],r9b
	fget_str [fp],rdx
	cmp	eax,-1
	je 	done
	inc	edi
	;put_str	rdx
	;put_str	endl
	;put_i	[rdx+RANK]
	;put_str	endl
	;put_i	[rdx+HEIGHT]
	;put_str	endl
	;put_i	[rdx+WEIGHT]
	;put_str	endl
	add	rdx, RECSIZE
	jmp	load
done:
	fclosem [fp]
	mov	r8, s
	mov	r9d, [r8+HEIGHT]
	mov	ebx, [r8+RANK]
	add	r8, RECSIZE
	mov	r10d, [r8+HEIGHT]
	mov	ecx, [r8+RANK]
	inc 	esi
	cmp	r9d, r10d
	jge	fg
	jmp	sg
fg:
	cmp	esi, edi
	je	fend
	inc	esi
	add	r8, RECSIZE
	mov	r10d, [r8+HEIGHT]
	mov	ecx, [r8+RANK]
	cmp	r9d, r10d
	jge	fg
	jmp	sg
sg:
	cmp	esi, edi
	je	send
	inc 	esi
	add	r8, RECSIZE
	mov	r9d, [r8+HEIGHT]
	mov	ebx, [r8+RANK]
	cmp	r9d, r10d
	jge	fg
	jmp	sg
send:
	put_str	tall
	put_i	ecx
	put_str	endl
	jmp	w
fend:
	put_str	tall
	put_i	ebx
	put_str	endl
	jmp	w
w:
	mov     r8, s
        mov     r9d, [r8+WEIGHT]
	mov	ebx, [r8+RANK]
        add     r8, RECSIZE
        mov     r10d, [r8+WEIGHT]
	mov	ecx, [r8+RANK]
	xor	esi, esi
        mov	esi, 1
        cmp     r9d, r10d
        jge     fwg
        jmp     swg
fwg:
        cmp     esi, edi
        je      fwend
        inc     esi
        add     r8, RECSIZE
        mov     r10d, [r8+WEIGHT]
	mov	ecx, [r8+RANK]
        cmp     r9d, r10d
        jge     fwg
        jmp     swg
swg:
        cmp     esi, edi
        je      swend
        inc     esi
        add     r8, RECSIZE
        mov     r9d, [r8+WEIGHT]
	mov	ebx, [r8+RANK]
        cmp     r9d, r10d
        jge     fwg
        jmp     swg
swend:
        sub     r8, RECSIZE
        put_str heavy
        put_i   ecx
        put_str endl
        jmp     exit
fwend:
        sub     r8, RECSIZE
        put_str heavy
        put_i   ebx
        put_str endl
        jmp     exit

exit:
	mov	ebx,0
	mov	eax,1
	int	80h
