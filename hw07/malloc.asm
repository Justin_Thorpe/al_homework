;; malloc.asm
;;
;; Stores sumo wrestlers using dynamic memory allocation
;;
;; By Justin T.
;;

%include "../lib/iomacros.asm"
%define	RANK	20
%define	HEIGHT	24
%define	WEIGHT	28
%define	RECSIZE	32

	global main	
	section .data

endl:	db	10,0
sfile:	db	"../data/short.txt",0
file:	db	"../data/sumo.txt",0
tall:	db	"The Tallest is: ",0
heavy:	db	"The Heaviest is: ",0

	section .bss
fp:	resq	1
q: 	resq	100

	section .text
main:
	;mov	r8, sfile
	mov	r8,file
	fopenr	[fp],r8
	mov	r14, q
	mov	r15, 0
	jmp	res
res:
	mov	rax, 9
	mov	rdi, 0
	mov	rsi, 32
	mov	rdx, 3
	mov	r10, 0x22
	mov	r8, -1
	mov	r9, 0
	syscall
	mov	r13, rax
	mov	[r14], r13

	fget_i [fp],[r13+RANK]
	fget_i [fp],[r13+HEIGHT]
	fget_i [fp],[r13+WEIGHT]
	fget_ch [fp],r9b
	fget_str [fp],r13

	cmp	eax, -1
	je	done
	inc	r15
	add	r14, 8
	jmp	res
done:
	fclosem [fp]
	mov 	r11, 0
	mov     r14, q
        mov     r12, [r14]
	inc 	r11
	mov	r8, [r14]
	mov	r9d, [r8+HEIGHT]
	mov	rcx, r8
	add	r14, 8
	mov	r8, [r14]
	mov	r10d, [r8+HEIGHT]
	mov	rdx, r8
	cmp	r9d, r10d
	jge	fg
	jmp	sg
	
fg:
	cmp	r11, r15 
	je	fend
	inc	r11
	add	r14, 8
	mov	r8, [r14]
	mov	r10d, [r8+HEIGHT]
	mov	rdx, r8
	cmp	r9d, r10d
	jge	fg
	jmp	sg
sg:
	cmp	r11, r15
	je	send
	inc 	r11
	add	r14, 8
	mov	r8, [r14]
	mov	r9d, [r8+HEIGHT]
	mov	rcx, r8
	cmp	r9d, r10d
	jge	fg
	jmp	sg

send:
	put_str	tall
	put_str	endl
        put_str rdx
        put_str endl
        put_i   [rdx+RANK]
        put_str endl
        put_i   [rdx+HEIGHT]
        put_str endl
        put_i   [rdx+WEIGHT]
        put_str endl
	jmp	w
fend:
	put_str tall
        put_str endl
        put_str rcx
        put_str endl
        put_i   [rcx+RANK]
        put_str endl
        put_i   [rcx+HEIGHT]
        put_str endl
        put_i   [rcx+WEIGHT]
        put_str endl
	jmp	w
w:
	put_str	endl
	mov	r11, 0
	mov	r14, q
	mov     r12, [r14]
        inc     r11
        mov     r8, [r14]
        mov     r9d, [r8+WEIGHT]
        mov     rcx, r8
        add     r14, 8
        mov     r8, [r14]
        mov     r10d, [r8+WEIGHT]
        mov     rdx, r8
        cmp     r9d, r10d
        jge     wfg
        jmp     wsg

wfg:
        cmp     r11, r15
        je      wfend
        inc     r11
        add     r14, 8
        mov     r8, [r14]
        mov     r10d, [r8+WEIGHT]
        mov     rdx, r8
        cmp     r9d, r10d
        jge     wfg
        jmp     wsg
wsg:
        cmp     r11, r15
        je      wsend
        inc     r11
        add     r14, 8
        mov     r8, [r14]
        mov     r9d, [r8+WEIGHT]
        mov     rcx, r8
        cmp     r9d, r10d
        jge     wfg
        jmp     wsg

wsend:
        put_str heavy
        put_str endl
        put_str rdx
        put_str endl
        put_i   [rdx+RANK]
        put_str endl
        put_i   [rdx+HEIGHT]
        put_str endl
        put_i   [rdx+WEIGHT]
        put_str endl
        jmp     exit
wfend:
        put_str heavy
        put_str endl
        put_str rcx
        put_str endl
        put_i   [rcx+RANK]
        put_str endl
        put_i   [rcx+HEIGHT]
        put_str endl
        put_i   [rcx+WEIGHT]
        put_str endl
        jmp     exit

exit:
	mov	ebx,0
	mov	eax,1
	int	80h
