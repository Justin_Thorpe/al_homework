;
; Builds nerd cards and uses bit operations to perform various actions.
;
; To compile:
; gcc -c readcodes.c
; nasm -f elf64 nerdcard.asm
; gcc nerdcard.o readcodes.o
; ./a.out
;
; A code record looks like this:
;
; Code {
;	int id;
;	int score;
;	char description[256];
; };
;
;
; A nerd record will look like this:
;
; Nerd {
;	char name[80];
;	long code;
; };
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define ID 0
%define SCORE 4
%define DESCRIPTION 8
%define NAME 0
%define CODE 17
%define RECSIZE 25 

		extern readCodes
		section .data
codefile:	db	"codes.txt",0
nerdfile:	db	"nerds.txt",0
space:		db	" ",0
endl		db	10,0
eHas:		db	"Achievements Everyone Has ...",0
nHas:		db	"Achievements Nobody Has ...",0
dash:		db	"------------------------------------",0

		section .bss
codes:		resq	64		; up to 64 codes are supported
coden:		resq	1		; number of nerds we have
nerds:		resb	RECSIZE*8	; number of nerds
fp:		resq	1		; file pointer
temp:		resq	1		; temp
		section .text
		global 		main
main:

		; coden= readCodes(codes,codefile)
		mov	rdi,codes
		mov	rsi,codefile
		call	readCodes
		mov	[coden],rax
		;put_i	[coden]
		;put_str	endl
		
		mov	rdi, nerds
		mov	rsi, nerdfile	
		call	readNerds
		call	displayNerds
		call	doneByAll
		call	doneByNone

theend:
		mov     eax, 60
		xor     rdi, rdi
		syscall

;--------------------------------------------------------------------------------
; doneByAll
;--------------
; displays achievemnts completed by nerds
;
doneByAll:
		put_str	dash
		put_str endl
		put_str	eHas
		put_str	endl
		put_str	dash
		put_str endl

		mov	rdx, nerds
		mov	rbx, nerds
		add	rbx, RECSIZE
		mov	rdi, 0		; counter for nerd loop
		mov	rsi, 1		; counter for elem #
compare:
		cmp	rdi, 6
		je	check
		mov	r11, qword[rdx+CODE]
		and	qword[rbx+CODE], r11
		add	rdx, RECSIZE
		add	rbx, RECSIZE
		inc	rdi
		jmp	compare
check:		
		cmp	qword[rbx+CODE], 0
		je	complete
		mov	rcx, 1
		and	rcx, qword[rbx+CODE]
		cmp	rcx, 0
		jg	exists
		shr	qword[rbx+CODE],1
		inc	rsi
		jmp	check
exists:
		mov	r15, codes
		mov	r14, rsi
		imul	r14, 264
		add	r15, r14
		;put_i	[r15+ID]
		;put_ch	[space]
		;put_i	[r15+SCORE]
		;put_ch	[space]
		add	r15,DESCRIPTION
		;put_str	r15
		;put_str	endl
		shr	qword[rbx+CODE],1
		inc	rsi
		jmp	check
complete:
		ret

;--------------------------------------------------------------------------------
; doneByNone
;--------------
; displays achievemnts no one has done
;
doneByNone:
		put_str dash
                put_str endl
                put_str nHas
                put_str endl
                put_str dash
                put_str endl

		ret

;--------------------------------------------------------------------------------
; displayNerds
;---------------
; displays nerds
;
displayNerds:
		mov     rdi, 0
                mov     rdx, nerds
print:
                cmp     rdi, 8
                je      stop
		put_i	edi
		put_ch	[space]
                put_str rdx
                put_ch	[space]
                put_i   [edx+CODE]
                put_str endl
                inc     rdi
                add     rdx, RECSIZE
                jmp     print
stop:
		ret

;--------------------------------------------------------------------------------
; readNerds
;---------------------
; builds a bit-string representing each nerd's achievements
;
readNerds:
		mov	r8, rsi  	;rsi currently has the flename
		mov	r13, rdi 	;address of nerd records array
		mov	rdi, 0 		;counter
		mov	r12, temp
		fopenr	[fp], r8
load:		

		fget_str [fp], r13
		cmp	eax, -1
		je	done
		mov	qword[r13+CODE], 0 	; clears out set
loop:
		fget_i [fp],[r12]		;temp for bit masking and inserting into set 
		cmp	eax, -1
		je	next
		;put_i	[r12d]
		;put_str	endl
		mov	rbx, 1
		mov	r10, 1		; counter for loop to shift since i have to use an immediate >:(
shift:		
		cmp	r10, [r12]
		je	finish
		shl	rbx, 1	; shifts 1 in temp to correct elem #
		inc	r10
		jmp	shift
finish:
		or	qword[r13+CODE], rbx	; adds elem # to set
		jmp	loop
next:
		add	r13, RECSIZE	; adds to jump to start of next record
		inc	rdi		; counter
		;put_i	edi
		;put_str	endl
		jmp	load
done:
		mov	rax, rdi
		fclosem [fp]
		ret

;--------------------------------------------------------------------------------
; displayCodes(codes,n)
;---------------------------
; display codes along with id numbers
;
displayCodes:
		xor	rcx,rcx

displayCodesLoop:
		cmp	rcx,rsi
		jge	endDisplayCodes
		mov	r8,[rdi+8*rcx]
		put_i	[r8+ID]
		put_ch	[space]
		put_i	[r8+SCORE]
		put_ch	[space]
		add	r8,DESCRIPTION
		put_str	r8
		put_str	endl
		inc	rcx
		jmp	displayCodesLoop

endDisplayCodes:
		mov	rax,rcx
		ret
