;;
;; lab day 01
;; 
;; Justin.T
;;

%include "../lib/dumpregs.asm"
	global main
	section .data

int: dd 68,-9
floaty: dd -68.125
	section .text

main:
	mov  	qword [int], 0x33333333
	mov	r8, int
	mov	r9, floaty
	mov	r10, [int]
	mov	r11, [floaty]
	mov 	rax, [int]
	mov	rbx, [int+8]
	mov	rcx, [int+16]
	mov	rdx, [int+24]
	mov	rsi, [int+32]
	mov	rdi, [int+40]
	;mov	r12, [int+48]
	mov	r13, [int+56]
	mov	r14, [int+64]
	
	dump_regs

	
	mov	eax, 60
	xor	rdi,rdi
	syscall
