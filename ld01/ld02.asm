;;
;; Lab day 02
;;
;; By justin thorpe
;;
;;

%include "../lib/iomacros.asm"
	global main
	section .data

random_int: dd 0
endl: db 10,0
guess_int: dd 0
guess_str: db "Guess a number between (1-100): ",0
high: db "Too high",0
low: db "Too low",0	

	section .text
main:
	add	r15, rbx
	add	r15, rcx
	add	r15, rdx
	add	r15, rsi
	add	r15, rdi
	add	r15, rbp
	add	r15, rsp
	add	r15, r9
	add	r15, r10
	add	r15, r11
	add 	r15, r12
	add	r15, r13
	add	r15, r14
	add 	r15, rax

	xor	rdx, rdx
	mov	rax, r15
	mov	r15, 100
	idiv	r15
	add	rdx,1 
	mov	[random_int], edx
	
	;put_i	[random_int]
	;put_str	endl
	
	put_str  guess_str
	get_i  [guess_int]
	;put_i  [guess_int]
	put_str  endl

	mov	eax, [random_int]
	mov	ebx, [guess_int]
	
start:
	cmp	eax, ebx
	jl	less

	cmp	eax, ebx
	je	end
	put_str	 high
	put_str  guess_str
	get_i	[guess_int]
	jmp	start



less:
	put_str  guess_str
        get_i  [guess_int]
	jmp	start	

end:
	mov	eax, 60
	xor	rdi, rdi
	syscall
